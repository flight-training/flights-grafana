FROM grafana/grafana
COPY conf/provisioning/ /etc/grafana/provisioning/
COPY dashboards/ /var/lib/grafana/dashboards